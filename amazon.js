"use strict";
const request = require("request-promise");

function makeSimpleCard(card) {
  return {
    type: "Simple",
    title: card.title,
    content: card.content
  };
}

function makeStandardCard(card) {
  return {
    type: "Standard",
    title: card.title,
    text: card.content,
    image: {
      smallImageUrl: card.smallImageUrl,
      largeImageUrl: card.largeImageUrl
    }
  };
}

/*
https://developer.amazon.com/public/solutions/alexa/alexa-skills-kit/docs/display-interface-reference
*/

function makeTextTemplate(config) {
  return {
    type: "Display.RenderTemplate",
    template: {
      type: "BodyTemplate1",
      token: config.token || "custom",
      backButton: config.backButton || "VISIBLE",
      backgroundImage: config.backgroundImage,
      title: config.title,
      textContent: config.textContent
    }
  };
}


function makeImageTemplate(config) {
  return {
    type: "Display.RenderTemplate",
    template: {
      type: `BodyTemplate${config.template}`,
      token: config.token || "custom",
      backButton: config.backButton || "VISIBLE",
      backgroundImage: config.backgroundImage,
      title: config.title,
      image: config.image,
      textContent: config.TextContent
    }
  };
}

function makeListTemplate(config) {
  return {
    type: "Display.RenderTemplate",
    template: {
      type: `ListTemplate${config.template}`,
      token: config.token || "custom",
      backButton: config.backButton || "VISIBLE",
      backgroundImage: config.backgroundImage,
      title: config.title,
      listItems: config.listItems
    }
  };
}

function getUserAddress(device_id, consent_token, postalCode) {
  return new Promise(function(resolve, reject) {
    let alexa_location_url = "";

    if(postalCode) {
      alexa_location_url = `https://api.amazonalexa.com/v1/devices/${device_id}/settings/address/countryAndPostalCode`;
    } else {
      alexa_location_url = `https://api.amazonalexa.com/v1/devices/${device_id}/settings/address`;
    }

    let opts = {
      method: "GET",
      url: alexa_location_url,
      headers: { Authorization: `Bearer ${consent_token}` }
    };

    request(opts).then((res) => {
      resolve(res);
    }).catch((err) => {
      reject(err);
    });
  })
}

function buildResponse(vw) {
  let response = {};
  let allAttributes = {};
  response.shouldEndSession = vw.response.shouldEndSession;
  response.reprompt = `<speak>${vw.response.reprompt}</speak>`;
  response.outputSpeech = `<speak>${vw.response.outputSpeech}</speak>`;

  if (vw.response.card) {
    response.card = vw.response.card;
  }

  Object.keys(vw.session_attributes).forEach((key) => {
    allAttributes[key] = vw.session_attributes[key];
  });

  Object.keys(vw.response.sessionAttributes).forEach((key) => {
    allAttributes[key] = vw.response.sessionAttributes[key];
  });

  if (this.response.directives.length !== 0) {
    response.directives = vw.response.directives;
  }

  return {
    version: "1.0",
    response: response,
    sessionAttributes: allAttributes
  };
}



module.exports = {
  makeSimpleCard: makeSimpleCard,
  makeStandardCard: makeStandardCard,
  makeTextTemplate: makeTextTemplate,
  makeImageTemplate: makeImageTemplate,
  makeListTemplate: makeListTemplate,
  buildResponse: buildResponse
};
