//https://github.com/jovotech/jovo-framework-nodejs/blob/master/lib/platforms/googleaction/googleActionResponse.js
//https://github.com/actions-on-google/actions-on-google-nodejs/blob/7bb4853703d818635a8ac74529372a3d89f41037/test/actions-sdk-app-test.js#L182

'use strict';

const SOURCE = "apiai-webhook";

function buildResponse(vw) {
  let response = {};

  response.richResponse = {items: []};

  let allAttributes = {};

  response.isSsml = true

  //open or close the mic
  response.expectUserResponse = vw.shouldEndSession;

  //add basic response
  response.richResponse.items.unshift({simpleResponse: {
    ssml: vw.outputSpeech
  }})

  // suggestions
  if (vw.suggestions) {
    vw.suggestions.forEach((suggestion) => {
      response.richResponse.suggestions.push({
        title: suggestion
      });
    })
  }

  //add reprompt if one exists
  if (data.reprompt) {
    response.noInputPrompts = [{ssml: vw.reprompt}]
  }

  // add existing session attributes
  Object.keys(data.session_attributes).forEach((key) => {
    allAttributes[key] = vw.session_attributes[key];
  });

  // add new session attributes
  Object.keys(data.response.sessionAttributes).forEach((key) => {
    allAttributes[key] = vw.response.sessionAttributes[key];
  });

  if (allAttributes) {
    response.conversationToken = JSON.stringify(allAttributes);
  }


  return {
    data: {
      google: response,
    },
    contextOut: [],
    source: SOURCE
  };
}


module.exports = {
  buildResponse: buildResponse
}
