"use strict";

const request = require('request-promise');

const amazon = require('./amazon');
const google = require('./google');


const AMAZON = "amazon";
const GOOGLE = "google";

class VoiceWrapper {
  constructor(event) {
    this.event = event;
    this.platform = this.checkPlatform(event);
    this.tempData = {};
    this.analytics = {};
    this.user = {};

    if (this.platform === GOOGLE) {
      this.event = JSON.parse(event.body);
      this.parseGoogleRequest(event);
    } else if (this.platform === AMAZON) {
      this.parseAmazonRequest(event);
    }

    if (this.platform === AMAZON) {
      this.response = {
        shouldEndSession: false,
        directives: [],
        card: null,
        sessionAttributes: {},
        reprompt: {
          outputSpeech: {
            type: "SSML",
            ssml: ""
          }
        },
        outputSpeech: {
          type: "SSML",
          ssml: ""
        }
      }
    }

    if (this.platform === GOOGLE) {
      this.response = {
        shouldEndSession: false,
        sessionAttributes: {},
        reprompt: null,
        outputSpeech: null,
        suggestions: []
      }
    }
  }

  /**
   * pass in your own user object
   * @param {object} user
  */
  setUser(user) {
    this.user = user || {};
  }

  /**
   * pass in your own analytics object
   * @param {object} analytics
   */
  initAnalytics(analytics) {
    this.analytics = analytics || {};
  }

  sendSessionAttributes(attr) {
    let keys = Object.keys(attr);
    keys.forEach(key => this.response.sessionAttributes[key] = attr[key]);
  }

  checkPlatform() {
    if (this.event.session) {
      return AMAZON;
    } else {
      return GOOGLE;
    }
  }

  setSpeech(ssml) {
    if (this.platform === AMAZON) {
      this.response.outputSpeech.ssml = ssml
    }

    if (this.platform === GOOGLE) {
      this.response.outputSpeech = ssml;
    }
  }

  endSession(end_sesion) {
    this.response.shouldEndSession = end_session;
  }

  setReprompt(ssml) {
    if (this.platform === AMAZON) {
      this.response.reprompt.outputSpeech.ssml = ssml;
    }

    if (this.platform === GOOGLE) {
      this.response.reprompt = ssml;
    }

  }

  setCard(card) {
    if (this.platform === AMAZON) {
      this.response.card = card;
    }
  }

  addDirective(directive) {
    if (this.platform === AMAZON) {
      this.response.directives.push(directive);
    }
  }

  addSuggestion(suggestion) {
    if (this.platform === GOOGLE) {
      this.response.suggestions.push(suggestion);
    }
  }

  getAddressTokens() {
    if (this.platform === AMAZON) {
      let device_id = this.device.deviceId;

      return {
        complete: device_id && this.permissions.consentToken,
        device_id: device_id,
        consent_token: consent_token
      }
    }
  }

  getResponseObject() {
    if (this.platform === AMAZON) {
      return amazon.buildResponse(this.response);
    } else {
      return google.buildResponse(this.response);
    }
  }

  parseGoogleRequest() {
    const ACTIONS_API_AI_CONTEXT = '_actions_on_gooogle_';
    const WELCOME_QUERY = 'GOOGLE_ASSISTANT_WELCOME';

    this.timestamp = this.event.timestamp;
    this.userId = this.event.originalRequest.data.user.userId;
    this.permissions = this.event.originalRequest.data.user.permissions || [];
    this.intentName = this.event.result.metadata.intentName;
    this.newSession = this.event.originalRequest.data.conversation.conversationToken === "[]";
    this.sessionId = this.event.sessionId;
    this.device = this.event.originalRequest.data.device;
    this.rawInput = this.event.originalRequest.data.inputs[0].rawInputs[0].query;
    this.launchRequest = this.event.result.resolveQuery === WELCOME_QUERY;
    this.slots = {};

    if (this.event.result.metadata.matchedParameters) {
      this.slots = this.event.result.metadata.matchedParameters;
    }

    this.session_attributes = {};

    if (this.event.originalRequest.data.conversation && this.event.originalRequest.data.conversation.conversationToken) {
      this.session_attributes = JSON.parse(this.event.originalRequest.data.conversation.conversationToken);
    }

    /*
    if(this.event.result.contexts.length > 0) {
      for (let i = 0; i < this.event.result.contexts.length; i++) {
        if (this.event.result.contexts[i].name === ACTIONS_API_AI_CONTEXT) {
          const parameters = this.event.result.contexts[i].parameters;
          if (parameters) {
            this.session_attributes = parameters;
          }
        }
      }
    }
    */
  }

  parseAmazonRequest() {
    const LAUNCH_REQUEST = 'LaunchRequest';

    this.timestamp = this.event.request.timestamp;
    this.userId = this.event.session.user.userId;
    this.permissions = this.event.session.user.permissions;

    if (this.event.request.intent) {
      this.intentName = this.event.request.intent.name;
    } else {
      this.intentName = LAUNCH_REQUEST;
    }

    this.slots = this.event.request.intent.slots;
    this.newSession = this.event.session.new;
    this.sessionId = this.event.session.sessionId;
    this.session_attributes = this.event.session.attributes;
    this.device = this.event.context.System.device;
    this.launchRequest = this.event.request.type === LAUNCH_REQUEST;
  }

  hasScreen() {
    if (this.platform === AMAZON) {
      if (Object.keys(this.event.context.System.device.supportedInterfaces).indexOf("Device") != -1) {
        return true;
      } else {
        return false;
      }
    } else {
      //TODO: handle Google case
    }
  }

  buildResponse() {
    if (this.platform === AMAZON) {
      return amazon.buildResponse(this);
    } else {
      return google.buildResponse(this);
    }
  }

}

module.exports = VoiceWrapper;
